const isRequired = (value) => {
  return value !== '' && value !== null && value !== undefined;
};

export function validateRequired(form, key, message) {
  const errors = [];
  if (!isRequired(form[key]))
    errors.push({
      key,
      message,
    });
  return errors;
}

export function validateBuildingSelected(form, t) {
  const errors = [];
  if (!isRequired(form.buildingName))
    errors.push({
      key: 'buildingName',
      message: t('errors.building_name'),
    });
  if (!isRequired(form.buildingAddress))
    errors.push({
      key: 'buildingAddress',
      message: t('errors.building_address'),
    });
  return errors;
}

export function validateBuilding(form, t) {
  const errors = [];
  if (!isRequired(form.buildingName))
    errors.push({
      key: 'buildingName',
      message: t('errors.building_name'),
    });
  if (!isRequired(form.buildingAddress))
    errors.push({
      key: 'buildingAddress',
      message: t('errors.building_address'),
    });
  if (!isRequired(form.aptNumber))
    errors.push({
      key: 'aptNumber',
      message: t('errors.apt_number'),
    });
  return errors;
}

export function validateContact(form, t) {
  const phoneNumberRegex =
    /^\(?([0-9]{3})\)?[-.●\s]?([0-9]{3})[-.●\s]?([0-9]{4})$/;

  const errors = [];
  if (!isRequired(form.firstName))
    errors.push({
      key: 'firstName',
      message: t('errors.first_name'),
    });
  if (!isRequired(form.lastName))
    errors.push({
      key: 'lastName',
      message: t('errors.last_name'),
    });
  if (!isRequired(form.whatsapp))
    errors.push({
      key: 'whatsapp',
      message: t('errors.whatsapp'),
    });

  return errors;
}
