import {
  validateRequired,
  validateBuilding,
  validateContact,
  validateBuildingSelected,
} from '../../util/validateForm';

export const startingSteps = {
  BUILDING_TYPE: {
    question: 'building_type_question',
    autoContinue: true,
    nextStep: 'BUILDING_NAME',
    validateStep: () => [],
    // validateStep: (form) =>
    //   validateRequired(form, 'buildingType', 'Building type is required'),
  },
  BUILDING_NAME: {
    question: 'building_name_question',
    nextStep: 'CONFIRM_BUILDING',
    previousStep: 'BUILDING_TYPE',
    validateStep: (form, t) =>
      validateRequired(form, 'buildingSearch', t('errors.building_search')),
  },
  CONFIRM_BUILDING: {
    question: 'confirm_address_question',
    autoContinue: true,
    nextStep: 'APT_NUMBER',
    previousStep: 'BUILDING_NAME',
    validateStep: (form, t) => validateBuildingSelected(form, t),
  },
  COLLECT_BUILDING_ADDRESS: {
    question: 'collect_building_question',
    nextStep: 'CONTACT_DETAILS',
    previousStep: 'CONFIRM_BUILDING',
    validateStep: (form, t) => validateBuilding(form, t),
  },
  APT_NUMBER: {
    question: 'apt_number_question',
    nextStep: 'CONTACT_DETAILS',
    previousStep: 'CONFIRM_BUILDING',
    validateStep: (form, t) =>
      validateRequired(form, 'aptNumber', t('errors.apt_number')),
  },
  CONTACT_DETAILS: {
    question: 'contact_details_question',
    previousStep: 'CONFIRM_BUILDING',
    nextStep: 'QUESTIONS',
    validateStep: (form, t) => validateContact(form, t),
  },
  QUESTIONS: {
    question: 'confirm_question',
    displaySubmitButton: true,
    previousStep: 'CONTACT_DETAILS',
    nextStep: 'CONFIRM_BOOKING',
  },
  CONFIRM_BOOKING: {
    question: '',
    autoContinue: true,
  },
};

export const optionsData = {
  buildingTypes: [
    {
      id: 'APT',
      title: 'Apartment',
      img: 'https://tailwindui.com/img/ecommerce/icons/icon-shipping-simple.svg',
    },
    {
      id: 'HOUSE',
      title: 'House',
      img: 'https://tailwindui.com/img/ecommerce/icons/icon-warranty-simple.svg',
      disabled: true,
    },
    {
      id: 'OFFICE',
      title: 'Office Building',
      img: 'https://tailwindui.com/img/ecommerce/icons/icon-exchange-simple.svg',
    },
    {
      id: 'OTHER',
      title: 'Other',
      img: 'https://tailwindui.com/img/ecommerce/icons/icon-shipping-simple.svg',
      disabled: true,
    },
  ],
  buildings: [
    {
      buildingName: 'Edificio Almenara',
      buildingAddress: 'Calle 12a, #30-185',
      neighborhood: 'El Poblado',
      city: 'Medellin',
    },
    {
      buildingName: 'Edificio Almenara',
      buildingAddress: 'Calle 305, #1234',
      neighborhood: 'Laureles',
      city: 'Medellin',
    },
  ],
  speeds: [
    { name: '200 Mbps', price: '50,000' },
    { name: '500 Mbps', price: '100,000' },
    { name: '1,000 Mbps (beta)', price: '400,000' },
  ],
};
