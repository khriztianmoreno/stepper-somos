const ButtonPrimary = ({ icon, onClick, fullWidth, children }) => {
  return (
    <button
      type="submit"
      className={`${
        fullWidth ? 'w-full flex justify-center' : 'inline-flex items-center'
      } px-4 py-2 border border-transparent text-sm font-medium rounded-sm text-black bg-white hover:bg-gray-100`}
      onClick={onClick}
    >
      {icon && <div className="-ml-1 mr-2 h-5 w-5">{icon}</div>}
      {children}
    </button>
  );
};

export default ButtonPrimary;
