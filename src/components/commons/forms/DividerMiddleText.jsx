const DividerMiddleText = ({ text, bgClass }) => {
  return (
    <div className="relative my-4">
      <div className="absolute inset-0 flex items-center" aria-hidden="true">
        <div className="w-full border-t border-white" />
      </div>
      <div className="relative flex justify-center">
        <span className={`px-2 text-sm text-white ${bgClass}`}>{text}</span>
      </div>
    </div>
  );
};

export default DividerMiddleText;
