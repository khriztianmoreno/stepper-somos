import { RadioGroup } from '@headlessui/react';

const RadioGroupCardContentsForBuilding = ({
  name,
  street,
  neighborhood,
  city,
}) => {
  return (
    <div className="text-sm font-sans">
      <RadioGroup.Label as="p" className="font-medium">
        {name}
      </RadioGroup.Label>
      <RadioGroup.Description as="div" className=" font-light">
        <p className="mt-1">{street}</p>
        <p className="mt-1">{`${neighborhood}, ${city}`}</p>
      </RadioGroup.Description>
    </div>
  );
};

export default RadioGroupCardContentsForBuilding;
