import { RadioGroup } from '@headlessui/react';

const RadioGroupCardContentsWithIcon = ({ title, img, active, checked }) => {
  return (
    <>
      <div className="flow-root">
        <img className="w-12 h-12" src={img} alt="" />
      </div>
      <RadioGroup.Label
        as="span"
        className="block text-md font-medium font-sans mt-2"
      >
        {title}
      </RadioGroup.Label>
    </>
  );
};

export default RadioGroupCardContentsWithIcon;
