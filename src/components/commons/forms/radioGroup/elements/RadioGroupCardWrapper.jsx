import { RadioGroup } from '@headlessui/react';

const RadioGroupCardWrapper = ({
  columns,
  selectedOption,
  setSelectedOption,
  children,
}) => {
  return (
    <RadioGroup value={selectedOption} onChange={setSelectedOption}>
      <div
        className={`grid gap-y-4 ${
          !columns ? 'grid-cols-1' : `grid-cols-${columns} gap-x-4`
        }`}
      >
        {children}
      </div>
    </RadioGroup>
  );
};

export default RadioGroupCardWrapper;
