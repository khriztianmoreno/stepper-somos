import { CheckCircleIcon } from '@heroicons/react/solid/index';
import { RadioGroup } from '@headlessui/react';

import classNames from '../../../../../util/classNames';

const RadioGroupOption = ({ option, children }) => {
  return (
    <RadioGroup.Option
      key={option.id}
      value={option}
      disabled={option.disabled}
      className={({ checked, active, disabled }) =>
        classNames(
          checked
            ? 'border-transparent bg-opacity-100'
            : 'border-white bg-opacity-20',
          active ? 'ring-2 ring-white' : '',
          'relative bg-white border rounded-sm p-4 flex cursor-pointer focus:outline-none',
          disabled && 'opacity-50 cursor-not-allowed',
        )
      }
    >
      {({ checked, active }) => (
        <>
          <div className="flex-1 flex">
            <div
              className={`flex flex-col ${
                checked ? 'text-black' : 'text-white'
              }`}
            >
              {children}
            </div>
          </div>

          {checked ? (
            <CheckCircleIcon
              className="h-5 w-5 text-black"
              aria-hidden="true"
            />
          ) : null}
          <div
            className={classNames(
              active ? 'border' : 'border-2',
              checked ? 'border-white' : 'border-transparent',
              'absolute -inset-px rounded-sm pointer-events-none',
            )}
            aria-hidden="true"
          />
        </>
      )}
    </RadioGroup.Option>
  );
};

export default RadioGroupOption;
