import RadioGroupCardWrapper from './elements/RadioGroupCardWrapper';
import RadioGroupOption from './elements/RadioGroupOption';
import RadioGroupCardContentsWithIcon from './elements/RadioGroupCardContentsWithIcon';

const RadioGroupCardsWithIcon = ({
  options,
  selectedOption,
  setSelectedOption,
  columns,
}) => {
  return (
    <RadioGroupCardWrapper
      options={options}
      selectedOption={selectedOption}
      setSelectedOption={setSelectedOption}
      columns={columns}
    >
      {options.map((option, idx) => {
        return (
          <RadioGroupOption option={option} key={idx}>
            <RadioGroupCardContentsWithIcon
              title={option.title}
              img={option.img}
            />
          </RadioGroupOption>
        );
      })}
    </RadioGroupCardWrapper>
  );
};

export default RadioGroupCardsWithIcon;
