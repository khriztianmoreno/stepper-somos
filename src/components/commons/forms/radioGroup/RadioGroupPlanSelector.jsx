import { RadioGroup } from '@headlessui/react';

import classNames from '../../../../util/classNames';

const RadioGroupPlanSelector = ({
  options,
  selected,
  setSelected,
  label,
  id,
}) => {
  return (
    <RadioGroup value={selected} onChange={setSelected} id={id}>
      <RadioGroup.Label className="sr-only">{label}</RadioGroup.Label>
      <div className="relative bg-white bg-opacity-20 rounded-sm -space-y-px">
        {options.map((option, i) => (
          <RadioGroup.Option
            key={option.name}
            value={option}
            className={({ checked }) =>
              classNames(
                i === 0 ? 'rounded-tl-sm rounded-tr-sm' : '',
                i === options.length - 1 ? 'rounded-bl-sm rounded-br-sm' : '',
                checked ? 'bg-white border-white z-10' : 'border-gray-200',
                'relative border p-4 flex flex-col cursor-pointer md:pl-4 md:pr-6 md:grid md:grid-cols-2 focus:outline-none',
              )
            }
          >
            {({ active, checked }) => (
              <>
                <div className="flex items-center text-sm">
                  <span
                    className={classNames(
                      checked
                        ? 'bg-black border-transparent'
                        : 'bg-white border-gray-300',
                      active ? 'ring-2 ring-offset-2 ring-black' : '',
                      'h-4 w-4 rounded-full border flex items-center justify-center',
                    )}
                    aria-hidden="true"
                  >
                    <span className="rounded-full bg-white w-1.5 h-1.5" />
                  </span>
                  <RadioGroup.Label
                    as="span"
                    className={classNames(
                      checked ? 'text-black' : 'text-white',
                      'ml-3 font-medium',
                    )}
                  >
                    {option.name}
                  </RadioGroup.Label>
                </div>
                <RadioGroup.Description className="ml-6 pl-1 text-sm md:ml-0 md:pl-0 md:text-right">
                  <span
                    className={classNames(
                      checked ? 'text-black' : 'text-gray-400',
                    )}
                  >
                    ${option.price} / mo
                  </span>{' '}
                </RadioGroup.Description>
              </>
            )}
          </RadioGroup.Option>
        ))}
      </div>
    </RadioGroup>
  );
};

export default RadioGroupPlanSelector;
