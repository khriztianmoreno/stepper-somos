import ButtonPrimary from './buttons/PrimaryButton';

const QuestionWrapper = ({
  question,
  description,
  includeContinueButton,
  handleContinueClick,
  children,
}) => {
  return (
    <div className="mt-10">
      <h2 className="text-lg font-medium text-gray-900">{question}</h2>
      {description && (
        <p className="mt-1 max-w-2xl text-sm text-gray-500">{description}</p>
      )}

      <div className="mt-4">{children}</div>
      {includeContinueButton && (
        <div className="mt-4">
          <ButtonPrimary fullWidth onClick={handleContinueClick}>
            Continue
          </ButtonPrimary>
        </div>
      )}
    </div>
  );
};

export default QuestionWrapper;
