const DataDisplayLine = ({ label, children }) => {
  return (
    <div className="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 font-sans">
      <dt className="text-sm font-medium text-white sm:col-span-1 leading-5">
        {label}
      </dt>
      <dd className="mt-1 flex text-sm text-gray-400 sm:mt-0 sm:col-span-2">
        <span className="flex-grow leading-5">{children}</span>
        <span className="ml-4 flex-shrink-0"></span>
      </dd>
    </div>
  );
};

export default DataDisplayLine;
