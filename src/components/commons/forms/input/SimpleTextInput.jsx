import InputWrapper from './InputWrapper';

const SimpleTextInput = ({
  value,
  label,
  name,
  id,
  placeholder,
  description,
  error,
  handleChange,
  disabled,
}) => {
  return (
    <InputWrapper label={label} description={description}>
      <input
        type="text"
        name={name}
        id={id}
        className={`focus:ring-white focus:border-white focus:z-10 block w-full sm:text-sm rounded-sm border-gray-500 bg-white bg-opacity-20 text-white placeholder-white placeholder-opacity-60`}
        placeholder={placeholder}
        onChange={handleChange}
        value={value}
        disabled={disabled}
      />
      <div className="text-sm font-sans text-orange-700 h-5">{error}</div>
    </InputWrapper>
  );
};

export default SimpleTextInput;
