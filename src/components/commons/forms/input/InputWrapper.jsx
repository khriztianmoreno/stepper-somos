const InputWrapper = ({ label, description, children }) => {
  return (
    <>
      {label && (
        <div className="flex justify-between">
          <label
            htmlFor=""
            className="block text-sm font-medium font-sans text-white"
          >
            {label}
          </label>
        </div>
      )}

      <div className="mt-1">{children}</div>

      {description && (
        <p className="mt-2 text-sm text-gray-500" id="helper-text">
          {description}
        </p>
      )}
    </>
  );
};

export default InputWrapper;
