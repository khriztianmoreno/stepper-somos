import React from 'react';
import InputWrapper from './InputWrapper';

const TextAreaInput = ({
  value,
  label,
  name,
  id,
  placeholder,
  description,
  handleChange,
  rows,
}) => {
  return (
    <InputWrapper label={label} description={description}>
      <textarea
        rows={rows ? rows : 4}
        name={name}
        id={id}
        className={`focus:ring-white focus:border-white focus:z-10 block w-full sm:text-sm rounded-sm border-gray-500 bg-white bg-opacity-20 text-white placeholder-white placeholder-opacity-60`}
        placeholder={placeholder}
        onChange={handleChange}
        value={value}
      />
    </InputWrapper>
  );
};

export default TextAreaInput;
