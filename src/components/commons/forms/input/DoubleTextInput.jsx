const DoubleTextInput = ({ leftInputData, rightInputData }) => {
  return (
    <>
      <div className="flex -space-x-px">
        <div className="w-1/2 flex-1 min-w-0 font-sans">
          <label
            htmlFor={leftInputData.id}
            className="block text-sm font-medium text-white"
          >
            {leftInputData.name}
          </label>
          <div className="mt-1">
            <input
              type="text"
              name={leftInputData.name}
              id={leftInputData.id}
              className={`focus:ring-white focus:border-white focus:z-10 block w-full sm:text-sm rounded-l-sm border-gray-500 bg-white bg-opacity-20 text-white placeholder-white placeholder-opacity-60`}
              placeholder={leftInputData.placeholder}
              value={leftInputData.value}
              onChange={leftInputData.changeHandler}
            />
          </div>
        </div>
        <div className="flex-1 min-w-0">
          <label
            htmlFor={rightInputData.id}
            className="block text-sm font-medium text-white"
          >
            {rightInputData.name}
          </label>
          <div className="mt-1">
            <input
              type="text"
              name={rightInputData.name}
              id={rightInputData.id}
              className={`focus:ring-white focus:border-white focus:z-10 block w-full sm:text-sm rounded-r-sm border-gray-500 bg-white bg-opacity-20 text-white placeholder-white placeholder-opacity-60`}
              placeholder={rightInputData.placeholder}
              value={rightInputData.value}
              onChange={rightInputData.changeHandler}
            />
          </div>
        </div>
      </div>
      <div className="text-sm font-sans text-orange-700 h-5">
        {leftInputData.error || rightInputData.error}
      </div>
    </>
  );
};

export default DoubleTextInput;
