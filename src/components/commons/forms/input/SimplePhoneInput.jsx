import 'react-phone-number-input/style.css';
import PhoneInput from 'react-phone-number-input';

const SimplePhoneInput = ({
  value,
  label,
  name,
  error,
  id,
  placeholder,
  defaultCountry,
  handleChange,
  international,
  disabled,
}) => {
  return (
    <>
      {label && (
        <div className="flex justify-between">
          <label
            htmlFor=""
            className="block text-sm font-medium font-sans text-white"
          >
            {label}
          </label>
        </div>
      )}
      <PhoneInput
        name={name}
        id={id}
        className="mt-1"
        international={international}
        defaultCountry={defaultCountry}
        placeholder={placeholder}
        onChange={(value) => handleChange({ target: { id, value } })}
        value={value}
        disabled={disabled}
      />
      <div className="text-sm font-sans text-orange-700 h-5">{error}</div>
    </>
  );
};

export default SimplePhoneInput;
