export const IS_LOADING = 'IS_LOADING';
export const LOAD_QUERY_DATA = 'LOAD_QUERY_DATA';
export const SET_FORM_DATA = 'SET_FORM_DATA';
export const SET_FORM_ERROR = 'SET_FORM_ERROR';
export const SET_SELECTED = 'SET_SELECTED';
export const SET_VALUE_TO_SEARCH = 'SET_VALUE_TO_SEARCH';
export const SET_STEP = 'SET_STEP';
