import { createContext, useContext, useReducer } from 'react';

import {
  IS_LOADING,
  LOAD_QUERY_DATA,
  SET_FORM_DATA,
  SET_FORM_ERROR,
  SET_SELECTED,
  SET_VALUE_TO_SEARCH,
  SET_STEP,
} from './types';

export const errorInitialState = {
  firstName: null,
  lastName: null,
  buildingSearch: null,
  email: null,
  phoneNumber: null,
  addressFull: null,
  aptNumber: null,
  city: null,
  state: null,
  postal: null,
  selectedPlan: null,
  whatsapp: null,
};

const AppStateContext = createContext();
const AppDispatchContext = createContext();

const initialState = {
  form: {
    buildingType: null,
    buildingName: '',
    buildingSearch: '',
    building: null,
    buildingAddress: '',
    addressNotListed: false,
    aptNumber: '',
    firstName: '',
    lastName: '',
    whatsapp: '',
    speed: null,
    questions: '',
    bookingConfirmed: false,
  },
  results: [],
  isLoading: false,
  selected: null,
  isDisabled: false,
  currentValueToSearch: null,
  step: 'BUILDING_TYPE',
  previousStep: null,
  progressStep: 1,
  errors: errorInitialState,
};

function AppReducer(state, action) {
  switch (action.type) {
    case LOAD_QUERY_DATA: {
      return {
        ...state,
        ...action.payload,
      };
    }
    case SET_STEP: {
      const { step, previousStep, progressStep, form, errors } = action.payload;
      return {
        ...state,
        form: {
          ...state.form,
          ...form,
        },
        errors: {
          ...state.errors,
          ...errors,
        },
        step,
        previousStep,
        progressStep: progressStep,
      };
    }
    case SET_SELECTED: {
      const selected = action.payload;
      return {
        ...state,
        selected,
        results: [selected],
        isDisabled: true,
      };
    }
    case SET_VALUE_TO_SEARCH: {
      return {
        ...state,
        currentValueToSearch: action.payload,
      };
    }
    case SET_FORM_DATA: {
      return {
        ...state,
        form: action.payload,
      };
    }
    case SET_FORM_ERROR: {
      return {
        ...state,
        errors: action.payload,
      };
    }
    case IS_LOADING: {
      return {
        ...state,
        isLoading: action.payload,
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

const AppProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, initialState);

  return (
    <AppStateContext.Provider value={state}>
      <AppDispatchContext.Provider value={dispatch}>
        {children}
      </AppDispatchContext.Provider>
    </AppStateContext.Provider>
  );
};

const useAppState = () => {
  const context = useContext(AppStateContext);
  if (context === undefined) {
    throw new Error('useAppState must be used within a AppProvider');
  }
  return context;
};

const useAppDispatch = () => {
  const context = useContext(AppDispatchContext);
  if (context === undefined) {
    throw new Error('useAppDispatch must be used within a AppProvider');
  }
  return context;
};

export { AppProvider, useAppState, useAppDispatch };
