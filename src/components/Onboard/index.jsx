import { ArrowLeftIcon, ArrowRightIcon } from '@heroicons/react/outline/index';
import { useTranslation } from 'react-i18next';

import { useAppDispatch, useAppState, errorInitialState } from '../store';
import { SET_STEP } from '../store/types';
import ButtonPrimary from '../commons/forms/buttons/PrimaryButton';
import { startingSteps } from '../../assets/data/onboard';

const TOTAL_STEPS = 7;

const OnboardStepper = ({
  question,
  description,
  displayContinueButton,
  displaySubmitButton,
  handleContinueClick,
  handleSubmitClick,
  children,
}) => {
  const { form, previousStep, progressStep } = useAppState();
  const dispatch = useAppDispatch();
  const { t } = useTranslation();

  let previousStepData = startingSteps[previousStep];

  const goBack = () => {
    dispatch({
      type: SET_STEP,
      payload: {
        form: {
          ...form,
          buildingAddress: '',
          aptNumber: '',
        },
        errors: errorInitialState,
        progressStep: progressStep - 1,
        step: previousStep,
        previousStep: previousStepData.previousStep,
      },
    });
  };

  const hadleSubmitClick = (e) => {
    e.preventDefault();
    handleContinueClick();
  };

  const progressPercentage = (progressStep * 100) / TOTAL_STEPS;
  return (
    <div className="bg-black pt-10">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div className="max-w-sm mx-auto pb-10">
          <div className="bg-white bg-opacity-50 rounded-full overflow-hidden">
            <div
              className="h-2 bg-white rounded-full"
              style={{ width: `${progressPercentage}%` }}
            />
          </div>
          <form className="" onSubmit={hadleSubmitClick}>
            <div className="mt-10">
              {question && (
                <h2 className="text-lg font-medium text-white font-sans">
                  {t(question)}
                </h2>
              )}
              {description && (
                <p className="mt-1 max-w-2xl text-sm text-gray-400">
                  {description}
                </p>
              )}

              <div className="mt-4">{children}</div>
              {displayContinueButton && (
                <div className="mt-6">
                  <ButtonPrimary fullWidth>
                    {t('controls.continue')}
                  </ButtonPrimary>
                </div>
              )}

              {displaySubmitButton && (
                <div className="mt-6">
                  <ButtonPrimary
                    fullWidth
                    onClick={handleSubmitClick}
                    icon={<ArrowRightIcon />}
                  >
                    {t('controls.confirm_installation')}
                  </ButtonPrimary>
                </div>
              )}
            </div>
          </form>
          {previousStep && (
            <div className="mt-10 margin-auto">
              <button
                onClick={goBack}
                type="button"
                className="inline-flex items-center px-2.5 py-1.5 border border-gray-300 shadow-sm text-xs font-medium rounded text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                <ArrowLeftIcon className="-ml-0.5 mr-2 h-4 w-4" />
                {t('controls.previous')}
              </button>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default OnboardStepper;
