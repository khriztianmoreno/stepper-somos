import { useTranslation } from 'react-i18next';

import CheckCircleIcon from '@heroicons/react/outline/CheckCircleIcon';

import TextAreaInput from '../commons/forms/input/TextAreaInput';
import DataDisplayLine from '../commons/forms/dataDisplay/DataDisplayLine';

const CollectQuestions = ({ data, setData }) => {
  const { t } = useTranslation();

  return (
    <>
      <div className="border-gray-800 border-b">
        <dl className="divide-y divide-gray-800">
          <DataDisplayLine label={t('fields.name')}>
            {data.firstName} {data.lastName}
          </DataDisplayLine>
          <DataDisplayLine label={t('fields.whatsapp')}>
            {data.whatsapp}
          </DataDisplayLine>
          <DataDisplayLine label={t('fields.building')}>
            {data.building ? data.building.name : data.buildingName}
            <br />
            {data.building ? data.building.street : data.buildingAddress}
            <br />
            {data.building &&
              `${data.building.neighborhood}, ${data.building.city}`}
          </DataDisplayLine>
          <DataDisplayLine label={t('fields.apt_number')}>
            {data.aptNumber}
          </DataDisplayLine>
          <DataDisplayLine label={t('fields.plan')}>
            <CheckCircleIcon className=" mr-2 h-5 w-5 inline-block" />
            FREE Installation
            <br />
            <CheckCircleIcon className=" mr-2 h-5 w-5 inline-block" />7 day free
            trial
            <br />
            <CheckCircleIcon className=" mr-2 h-5 w-5 inline-block" />
            Starting at $50,000/month
            <br />
            <CheckCircleIcon className=" mr-2 h-5 w-5 inline-block" />
            Cancel anytime
            <br />
          </DataDisplayLine>
        </dl>
      </div>
      <div className="mt-6 mb-6">
        <TextAreaInput
          rows={4}
          label={t('fields.questions')}
          id="questions"
          name="questions"
          placeholder=""
          handleChange={setData}
          value={data.questions}
        />
      </div>
    </>
  );
};

export default CollectQuestions;
