import RadioGroupPlanSelector from '../commons/forms/radioGroup/RadioGroupPlanSelector';

const SelectSpeed = ({ options, data, setData }) => {
  const speedId = 'speed';

  return (
    <RadioGroupPlanSelector
      id={speedId}
      options={options}
      selected={data.speed}
      setSelected={(option) => {
        setData({
          speed: option,
        });
      }}
      label="Speed"
    />
  );
};

export default SelectSpeed;
