import { useTranslation } from 'react-i18next';

const BookingConfirmed = () => {
  const { t } = useTranslation();

  return (
    <div className="h-96 inline-flex flex-col justify-center items-center">
      <h1 className="text-xl text-center font-normal text-white font-sans mb-10">
        {t('booking_question')}
      </h1>
      <a
        className="w-3/4 px-4 py-2 text-center border border-transparent text-xl font-normal rounded-sm text-black bg-white hover:bg-gray-100"
        href="https://www.somosinternet.co/"
      >
        {t('controls.finish')}
      </a>
    </div>
  );
};

export default BookingConfirmed;
