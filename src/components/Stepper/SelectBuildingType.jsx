import RadioGroupCardsWithIcon from '../commons/forms/radioGroup/RadioGroupCardsWithIcon';
import QuestionWrapper from '../commons/forms/QuestionWrapper';

const SelectBuildingType = ({
  question,
  description,
  options,
  data,
  setData,
}) => {
  return (
    <QuestionWrapper question={question} description={description}>
      <RadioGroupCardsWithIcon
        options={options}
        selectedOption={data.buildingType}
        setSelectedOption={(option) => {
          setData({
            buildingType: option?.title,
          });
        }}
        columns={2}
      />
    </QuestionWrapper>
  );
};

export default SelectBuildingType;
