import { useTranslation } from 'react-i18next';

import SimpleTextInput from '../commons/forms/input/SimpleTextInput';

const CollectBuildingAddress = ({ errors, data, setData }) => {
  const { t } = useTranslation();

  return (
    <>
      <SimpleTextInput
        label={t('fields.building_name')}
        id="buildingName"
        error={errors['buildingName']}
        placeholder=""
        handleChange={setData}
        value={data.buildingName}
      />
      <div className="mt-4">
        <SimpleTextInput
          label={t('fields.building_address')}
          id="buildingAddress"
          placeholder=""
          error={errors['buildingAddress']}
          handleChange={setData}
          value={data.buildingAddress}
        />
      </div>
      <div className="mt-4">
        <SimpleTextInput
          label={t('fields.apt_number')}
          id="aptNumber"
          error={errors['aptNumber']}
          placeholder=""
          handleChange={setData}
          value={data.aptNumber}
        />
      </div>
    </>
  );
};

export default CollectBuildingAddress;
