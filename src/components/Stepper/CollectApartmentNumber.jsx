import React from 'react';
import { useTranslation } from 'react-i18next';

import SimpleTextInput from '../commons/forms/input/SimpleTextInput';

const CollectApartmentNumber = ({ errors, data, setData }) => {
  const { t } = useTranslation();

  return (
    <SimpleTextInput
      label={t('fields.apt_number')}
      id="aptNumber"
      error={errors['aptNumber']}
      placeholder=""
      handleChange={setData}
      value={data.aptNumber}
    />
  );
};

export default CollectApartmentNumber;
