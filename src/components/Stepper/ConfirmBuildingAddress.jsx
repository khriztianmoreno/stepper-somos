import { OfficeBuildingIcon } from '@heroicons/react/outline/index';
import { useTranslation } from 'react-i18next';

import { SET_FORM_DATA } from '../store/types';
import RadioGroupOption from '../commons/forms/radioGroup/elements/RadioGroupOption';
import RadioGroupCardWrapper from '../commons/forms/radioGroup/elements/RadioGroupCardWrapper';
import RadioGroupCardContentsForBuilding from '../commons/forms/radioGroup/elements/RadioGroupCardContentsForBuilding';
import DividerMiddleText from '../commons/forms/DividerMiddleText';
import ButtonPrimary from '../commons/forms/buttons/PrimaryButton';
import { useAppDispatch, useAppState } from '../store';
import { useEffect } from 'react';

const ConfirmBuildingAddress = ({
  options,
  data,
  handleContinue,
  handleAddressNotListed,
}) => {
  const { t } = useTranslation();
  const { form } = useAppState();
  const dispatch = useAppDispatch();

  const handelSelectedBuilding = (building) => {
    dispatch({
      type: SET_FORM_DATA,
      payload: { ...form, ...building },
    });
  };

  useEffect(() => {
    if (form.buildingAddress) {
      handleContinue();
    }
  }, [form.buildingAddress, handleContinue]);

  return (
    <>
      <RadioGroupCardWrapper
        options={options}
        selectedOption={data.building}
        setSelectedOption={handelSelectedBuilding}
      >
        {options.map((option, idx) => {
          return (
            <RadioGroupOption option={option} key={idx}>
              <RadioGroupCardContentsForBuilding
                name={option.buildingName}
                street={option.buildingAddress}
                neighborhood={option.neighborhood}
                city={option.city}
              />
            </RadioGroupOption>
          );
        })}
      </RadioGroupCardWrapper>
      <DividerMiddleText text={t('or')} bgClass="bg-black" />
      <div className="text-center">
        <h3 className="mt-2 text-sm font-medium font-sans text-gray-400">
          {t('no_found_address')}
        </h3>
        <div className="mt-4">
          <ButtonPrimary
            icon={<OfficeBuildingIcon />}
            onClick={handleAddressNotListed}
            fullWidth
          >
            {t('controls.building_no_found')}
          </ButtonPrimary>
        </div>
      </div>
    </>
  );
};

export default ConfirmBuildingAddress;
