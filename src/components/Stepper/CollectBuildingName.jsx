import { useTranslation } from 'react-i18next';

import SimpleTextInput from '../commons/forms/input/SimpleTextInput';

const CollectBuildingName = ({ errors, data, setData }) => {
  const { t } = useTranslation();

  return (
    <SimpleTextInput
      label={t('fields.building_name')}
      id="buildingSearch"
      error={errors['buildingSearch']}
      placeholder=""
      handleChange={setData}
      value={data.buildingSearch}
    />
  );
};

export default CollectBuildingName;
