import { useTranslation } from 'react-i18next';

import DoubleTextInput from '../commons/forms/input/DoubleTextInput';
import SimplePhoneInput from '../commons/forms/input/SimplePhoneInput';

const CollectContactDetails = ({ errors, data, setData }) => {
  const { t } = useTranslation();

  return (
    <>
      <DoubleTextInput
        leftInputData={{
          name: t('fields.first_name'),
          id: 'firstName',
          value: data.firstName,
          changeHandler: setData,
          placeholder: '',
          error: errors['firstName'],
        }}
        rightInputData={{
          name: t('fields.last_name'),
          id: 'lastName',
          value: data.lastName,
          changeHandler: setData,
          placeholder: '',
          error: errors['lastName'],
        }}
      />
      <div className="mt-4">
        <SimplePhoneInput
          label={t('fields.whatsapp')}
          id="whatsapp"
          placeholder=""
          error={errors['whatsapp']}
          international
          defaultCountry="CO"
          handleChange={setData}
          value={data.whatsapp}
        />
      </div>
    </>
  );
};

export default CollectContactDetails;
