import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import { AppProvider } from './components/store';
import { I18nextProvider } from 'react-i18next';
import i18n from './i18n';

import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <AppProvider>
      <I18nextProvider i18n={i18n}>
        <App />
      </I18nextProvider>
    </AppProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);
