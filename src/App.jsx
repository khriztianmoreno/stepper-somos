import { useTranslation } from 'react-i18next';

import {
  SET_FORM_DATA,
  SET_STEP,
  SET_FORM_ERROR,
} from './components/store/types';
import SelectBuildingType from './components/Stepper/SelectBuildingType';
import CollectBuildingName from './components/Stepper/CollectBuildingName';
import ConfirmBuildingAddress from './components/Stepper/ConfirmBuildingAddress';
import CollectApartmentNumber from './components/Stepper/CollectApartmentNumber';
import CollectContactDetails from './components/Stepper/CollectContactDetails';
import OnboardStepper from './components/Onboard';
import BookingConfirmed from './components/Stepper/BookingConfirmed';
import CollectQuestions from './components/Stepper/CollectQuestions';
import CollectBuildingAddress from './components/Stepper/CollectBuildingAddress';

import { optionsData, startingSteps } from './assets/data/onboard';
import { useAppDispatch, useAppState } from './components/store';

const Onboard = () => {
  const { t } = useTranslation();
  const { form, step, progressStep, errors } = useAppState();
  const dispatch = useAppDispatch();

  let activeStepData = startingSteps[step];
  let previousStepData = startingSteps[activeStepData.nextStep];

  const _setData = (newData) => {
    dispatch({
      type: SET_FORM_DATA,
      payload: { ...form, ...newData },
    });

    //manually handle the continue button click if there is no continue button e.g. radio groups
    if (activeStepData.autoContinue) {
      handleContinueClick();
    }
  };

  const onInputChange = (e) => {
    delete errors[e.target.id];
    dispatch({
      type: SET_FORM_ERROR,
      payload: {
        ...errors,
      },
    });
    _setData({
      [e.target.id]: e.target.value,
    });
  };

  const handleAddressNotListed = () => {
    dispatch({
      type: SET_FORM_DATA,
      payload: {
        ...form,
        buildingName: '',
        buildingAddress: '',
        aptNumber: '',
      },
    });
    dispatch({
      type: SET_STEP,
      payload: {
        step: 'COLLECT_BUILDING_ADDRESS',
        previousStep: previousStepData.previousStep,
      },
    });
  };

  const handleSubmitClick = () => {
    dispatch({
      type: SET_STEP,
      payload: {
        step: 'CONFIRM_BOOKING',
        progressStep: progressStep + 1,
        previousStep: null,
      },
    });
  };

  const handleContinueClick = () => {
    const formErrors = activeStepData
      .validateStep(form, t)
      .reduce((acc, error) => {
        acc[error.key] = error.message;
        return acc;
      }, {});

    if (Object.keys(formErrors).length) {
      dispatch({
        type: SET_FORM_ERROR,
        payload: {
          ...formErrors,
        },
      });
    } else {
      dispatch({
        type: SET_STEP,
        payload: {
          step: activeStepData.nextStep,
          progressStep: progressStep + 1,
          previousStep: previousStepData.previousStep,
        },
      });
    }
  };

  const getNextStep = () => {
    switch (step) {
      case 'BUILDING_NAME':
        return (
          <CollectBuildingName
            errors={errors}
            data={form}
            setData={onInputChange}
          />
        );
      case 'CONFIRM_BUILDING':
        return (
          <ConfirmBuildingAddress
            options={optionsData.buildings}
            data={form}
            handleAddressNotListed={handleAddressNotListed}
            handleContinue={handleContinueClick}
          />
        );
      case 'COLLECT_BUILDING_ADDRESS':
        return (
          <CollectBuildingAddress
            errors={errors}
            data={form}
            setData={onInputChange}
          />
        );
      case 'APT_NUMBER':
        return (
          <CollectApartmentNumber
            errors={errors}
            data={form}
            setData={onInputChange}
          />
        );
      case 'CONTACT_DETAILS':
        return (
          <CollectContactDetails
            errors={errors}
            data={form}
            setData={onInputChange}
          />
        );
      case 'QUESTIONS':
        return <CollectQuestions data={form} setData={onInputChange} />;
      case 'CONFIRM_BOOKING':
        return <BookingConfirmed data={form} />;
      default:
        return (
          <SelectBuildingType
            options={optionsData.buildingTypes}
            data={form}
            setData={_setData}
          />
        );
    }
  };

  return (
    <OnboardStepper
      question={activeStepData.question}
      description={activeStepData.description}
      displayContinueButton={
        !activeStepData.autoContinue && !activeStepData.displaySubmitButton
      }
      displaySubmitButton={activeStepData.displaySubmitButton}
      displayBackButton={activeStepData.previousStep}
      handleContinueClick={handleContinueClick}
      handleSubmitClick={handleSubmitClick}
    >
      {getNextStep()}
    </OnboardStepper>
  );
};

export default Onboard;
